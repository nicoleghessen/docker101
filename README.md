## Docker and containerisation


#### What is a container?
We can use containers to build, share and run our applications
A container is a standard unit of software that packages up code and all its dependencies so the application runs quickly and reliably from one computing environment to another. 

Containers isolate software from its environment and ensure that it works uniformly despite differences for instance between development and staging.

#### How is it different from a VM?
Containers and virtual machines have similar resource isolation and allocation benefits, but function differently because containers virtualize the operating system instead of hardware. Containers are more portable and efficient.

Containers take up less space than VMs, can handle more applications and require fewer VMs and Operating systems.

#### How is it similar?


#### What is Docker?

A Docker container image is a lightweight, standalone, executable package of software that includes everything needed to run an application: code, runtime, system tools, system libraries and settings.


#### What other services of containers exist?

Docker is the standard but others do exist.

K8s and EKS and PODMAN are container orchestration and deployment tools.


#### Main sections of docker?



## Docker

Docker is the containerisation tool.
It pulls images from dockerhub.

Docker Hub hosts many images! Like nginx or httpd.

main commands:

```bash
# To start a container use docker run
$ docker run -d -p 80:80 docker/getting-started
# -d is for dettatched mode
# -p is for port mapping [outside_container]:[inside_container]

# check containers running
$ docker ps


# stop a container
$ docker stop [container_id_or_name]

# docker images to see images in computer
$ docker images


```

**task**

- go get an image of httpd 
- how can you "ssh" into your docker container 
- have a look around
- look at option `-dit` what does that do?

- Start another server with nginx on a different port 