# We want a Redhat-Centos httpd server

# Get a base container with Redhat-Centos installed
# FROM redhat/ubi8-minimal:latest
FROM centos:centos7.9.2009


# Run a command to install httpd
RUN yum -y install httpd

# Add our own content

COPY index.html /var/www/html


# Explicitly mark port 80 as exposed, more for K8s and Docker compose that needs to know where services are running to connect to other containers.
EXPOSE 80

# A command that will run everytime a container is launched that starts httpd service
# Entrypoint like user data for ami's

ENTRYPOINT ["httpd", "-DFOREGROUND"]



